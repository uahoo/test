#!/bin/sh


mkdir -p /Library/StartupItems/qbagent
cat <<EOF >/Library/StartupItems/qbagent/StartupParameters.plist
<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE plist PUBLIC "-//Apple Computer//DTD PLIST 1.0//EN" "http://www.apple.com/DTDs/PropertyList-1.0.dtd">
<plist version="1.0">
<dict>
       <key>Description</key>
       <string>qbagent</string>
       <key>Messages</key>
       <dict>
               <key>start</key>
               <string>Starting qbagent</string>
               <key>stop</key>
               <string>Stopping qbagent</string>
       </dict>
       <key>Provides</key>
       <array>
               <string>qbagent</string>
       </array>
       <key>Requires</key>
       <array>
               <string>IPFilter</string>
       </array>
</dict>
</plist>
EOF

cat <<EOF >/Library/StartupItems/qbagent/qbagent
#!/bin/sh

. /etc/rc.common
#. /etc/ossec-init.conf
#if [ "X\${DIRECTORY}" = "X" ]; then
#    DIRECTORY="/var/ossec"
#fi
PATH="/usr/local/sbin:/usr/local/bin:/sbin:/bin:/usr/sbin:/usr/bin:$PATH"

StartService ()
{
       sudo  -u  build_mac_mini sh -c '/Users/build_mac_mini/buildagent/bin/agent.sh start' >/tmp/qblog
}

StopService ()
{
        sudo  -u  build_mac_mini sh -c '/Users/build_mac_mini/buildagent/bin/agent.sh stop'
}

RestartService ()
{
         sudo  -u  build_mac_mini sh -c '/Users/build_mac_mini/buildagent/bin/agent.sh restart'
}

#RunService "\$1"

EOF
chmod 755 /Library/StartupItems/qbagent
chmod 644 /Library/StartupItems/qbagent/StartupParameters.plist
chmod 755 /Library/StartupItems/qbagent/qbagent